from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
response = {}

def index(request):
    if 'email' in request.session :
            print('logged in')
            print('di login')
            response['login'] = True
            return HttpResponseRedirect(reverse('profil_user:index'))
    else :
            print('not logged in')
            response['login'] = False
            html = 'homepage.html'
            return render(request, html, response)

def login(request):
    if 'email' in request.session :
            print('logged in')
            response['login'] = True
            return HttpResponseRedirect(reverse('profil_user:index'))
    else :
            print('not logged in')
            response['login'] = False
            html = 'login.html'
            return render(request, html, response)
