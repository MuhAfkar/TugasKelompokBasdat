from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from .views import response
from .db_helper import KoneksiDb

# authentication
def auth_login(request):

    if request.method == "POST":
        email = request.POST.get('email')
        password = request.POST.get('password')
        conn = KoneksiDb()
        check_user = conn.check_account(email,password)
        role_user = conn.get_role(email)

        if check_user == True:
            messages.info(request, 'Login success')
            request.session['email'] = email
            request.session['password'] = password
            request.session['role'] = role_user
            response['role'] = role_user
        else:
            messages.error(request, "Username atau password salah")
            return HttpResponseRedirect(reverse('login:login'))
    return HttpResponseRedirect(reverse('profil_user:index'))

def auth_logout(request):
        request.session.flush()
        messages.info(request, "Anda berhasil logout")
        return HttpResponseRedirect(reverse('profil_user:index'))




