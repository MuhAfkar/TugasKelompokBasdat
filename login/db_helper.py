import psycopg2
from django.contrib import messages

class KoneksiDb:
    def __init__(self):
        try:
            self.connection = psycopg2.connect("host='dbpg.cs.ui.ac.id' dbname='db076' user='db076' password='phi4omiD'")
            self.connection.autocommit = True
            self.cursor = self.connection.cursor()
        except:
            print("Can't connect")

    def check_account(self,email,password):
        self.cursor.execute("SET search_path to SION;")
        self.cursor.execute("SELECT exists(SELECT email,password  FROM sion.user where email=%s and password=%s);",[email,password])
        row = self.cursor.fetchone()
        return row[0] is True

    def get_role(self,email):
        roles = ["donatur","sponsor","relawan","pengurus_organisasi"]
        self.cursor.execute("SET search_path to SION;")
        for role in roles:
            self.cursor.execute("SELECT exists(SELECT email FROM " + role + " WHERE email=%s);",[email])
            row = self.cursor.fetchone()
            if row[0] is True :
                return role

    def insert_regis_user(self, role, email, password, nama, alamat_lengkap, tanggal_lahir, nomor_handphone, keahlian, logo_sponsor):
        self.cursor.execute("SET search_path to SION;")
        if role.lower() == 'donatur':
            if role == '' or email == '' or password == '' or nama == '' or alamat_lengkap == '':
                messages.add_message(request, messages.INFO, 'Data belum dimasukkan dengan benar')
            else:
                self.cursor.execute("INSERT INTO sion.user VALUES(%s,%s,%s,%s)",[email,password,nama,alamat_lengkap])
                self.cursor.execute("INSERT INTO donatur VALUES(%s,0)",[email])
        elif role.lower() == 'sponsor':
            if role == '' or email == '' or password == '' or nama == '' or alamat_lengkap == '' or logo_sponsor == '':
                messages.add_message(request, messages.INFO, 'Data belum dimasukkan dengan benar')
            else:
                self.cursor.execute("INSERT INTO sion.user VALUES(%s,%s,%s,%s)",[email,password,nama,alamat_lengkap])
                self.cursor.execute("INSERT INTO sponsor VALUES(%s,%s)",[email,logo_sponsor])
        else:
            if role == '' or email == '' or password == '' or nama == '' or alamat_lengkap == '' or tanggal_lahir == '' or nomor_handphone == '' or keahlian == '':
                messages.add_message(request, messages.INFO, 'Data belum dimasukkan dengan benar')
            else:
                self.cursor.execute("INSERT INTO sion.user VALUES(%s,%s,%s,%s)",[email,password,nama,alamat_lengkap])
                self.cursor.execute("INSERT INTO relawan VALUES(%s,%s,%s)",[email, nomor_handphone, tanggal_lahir])
                self.cursor.execute("INSERT INTO keahlian_relawan VALUES(%s,%s)",[email, keahlian])

    def insert_regis_organisasi(self, email, website, nama, provinsi, kabupaten_kota, kecamatan, kelurahan, kode_pos, nama_pengurus, email_pengurus, alamat_pengurus):
        self.cursor.execute("SET search_path to SION;")
        if email.lower() == '':
            if website == '' or nama == '' or nama == '' or provinsi == '' or kabupaten_kota == '' or kecamatan == '' or kelurahan == '' or kode_pos == '' or nama_pengurus == '' or email_pengurus == '' or alamat_pengurus == '':
                messages.add_message(request, messages.INFO, 'Data belum dimasukkan dengan benar')
            else:
                self.cursor.execute("INSERT INTO donatur VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",[email, website, nama, provinsi, kabupaten_kota, kecamatan, kelurahan, kode_pos, nama_pengurus, email_pengurus, alamat_pengurus])
