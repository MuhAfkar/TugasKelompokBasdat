from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from login.db_helper import KoneksiDb

# Create your views here.
response = {}

def index(request):
        if 'email' in request.session :
                print('logged in')
                print('di profil')
                response['login'] = True

                conn = KoneksiDb()
                conn.cursor.execute("select * from sion.user where email='"+request.session['email']+"';")
                row = conn.cursor.fetchone()
                response['email'] = row[0]
                response['nama'] = row[2]
                response['alamat'] = row[3]
                response['role'] = request.session['role']
                html = 'profil_user.html'
                return render(request, html, response)

        else :
                print('not logged in')
                response['login'] = False
                return HttpResponseRedirect(reverse('login:index'))
