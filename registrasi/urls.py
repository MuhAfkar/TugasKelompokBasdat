from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^pilih_role/$',index,name='index'),
    url(r'^$',register_page,name='regis'),
    url(r'^insert_regis',konfirmasi_registrasi,name='konfirmasi'),
    url(r'^insert_organisasi',konfirmasi_registrasi_org,name='konfirmasi_org'),
    url(r'^data_organisasi',halaman_habis_regis_org,name='abis_org'),
]
