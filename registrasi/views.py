from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from login.db_helper import KoneksiDb
from django.contrib import messages

# Create your views here.
response = {}

def index(request):
    if 'email' in request.session :
        print('logged in')
        response['login'] = True
        return HttpResponseRedirect(reverse('profil_user:index'))
    else :
        print('mau regis')
        response['login'] = False
        html = 'pilih_role.html'
        return render(request, html, response)

def register_page(request):
    if 'email' in request.session :
        print('logged in')
        response['login'] = True
        return render(request,'styleguide.html',response)
    else:
        if request.method == "POST":
            role = request.POST.get('role_user')
            request.session['role'] = role
            if role == 'Donatur':
                html = 'register_donatur.html'
                return render(request, html, response)
            if role == 'Relawan':
                html = 'register_relawan.html'
                return render(request, html, response)
            if role == 'Sponsor':
                html = 'register_sponsor.html'
                return render(request, html, response)
            if role == 'Pengurus Organisasi':
                html = 'register_organisasi.html'
                return render(request, html, response)

def konfirmasi_registrasi(request):
    if request.method == "POST":
        role = request.session['role']
        email = request.POST.get('email')
        password = request.POST.get('password')
        nama = request.POST.get('nama')
        kecamatan = request.POST.get('kecamatan')
        kabupaten = request.POST.get('kabupaten')
        provinsi = request.POST.get('propinsi')
        kode_pos = request.POST.get('kode_pos')
        jalan_no_rumah = request.POST.get('jalan')
        alamat_lengkap = "{}".format(kecamatan) + ", " + "{}".format(kabupaten) + ", " + "{}".format(provinsi) + ", " + "{}".format(jalan_no_rumah) + ". Kode Pos: " + "{}".format(kode_pos)
        conn = KoneksiDb()

        if role.lower() == 'donatur':
            conn.insert_regis_user(role, email, password, nama, alamat_lengkap, '', '', '', '')
        elif role.lower() == 'sponsor':
            logo = request.POST.get('logo')
            conn.insert_regis_user(role, email, password, nama, alamat_lengkap, '', '', '', logo)
        else:
            tanggal_lahir = request.POST.get('tanggal_lahir')
            nomor_handphone = request.POST.get('nomor_handphone')
            keahlian = request.POST.get('keahlian')
            conn.insert_regis_user(role, email, password, nama, alamat_lengkap, tanggal_lahir, nomor_handphone, keahlian, '')


        messages.info(request, 'Registrasi success')
        request.session['email'] = email
        request.session['password'] = password
        request.session['role'] = role.lower()
        response['role'] = role.lower()
        response['login'] = True
        return HttpResponseRedirect(reverse('profil_user:index'))
    return HttpResponseRedirect(reverse('login:index'))

def konfirmasi_registrasi_org(request):
    if request.method == "POST":
        email = request.POST.get('email')
        website = request.POST.get('website')
        nama = request.POST.get('nama')
        provinsi = request.POST.get('provinsi')
        kabupaten_kota = request.POST.get('kabupaten_kota')
        kecamatan = request.POST.get('kecamatan')
        kelurahan = request.POST.get('kelurahan')
        kode_pos = request.POST.get('kode_pos')
        nama_pengurus = request.POST.get('nama_pengurus')
        email_pengurus = request.POST.get('email_pengurus')
        alamat_pengurus = request.POST.get('alamat_pengurus')
        alamat_lengkap = "{}".format(kecamatan) + ", " + "{}".format(kabupaten_kota) + ", " + "{}".format(provinsi) + ", " + "{}".format(kelurahan) + ". Kode Pos: " + "{}".format(kode_pos)
        conn = KoneksiDb()

        conn.insert_regis_organisasi(email, website, nama, provinsi, kabupaten_kota, kecamatan, kelurahan, kode_pos, nama_pengurus, email_pengurus, alamat_pengurus)
        conn.cursor.execute("INSERT INTO organisasi VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s)",[email,website,nama,provinsi,kabupaten_kota,kecamatan,kelurahan,kode_pos,'True'])
        response['email'] = email
        response['website'] = website
        response['nama'] = nama
        response['alamat_lengkap'] = alamat_lengkap
        response['nama_pengurus'] = nama_pengurus
        response['email_pengurus'] = email_pengurus
        response['alamat_pengurus'] = alamat_pengurus
        conn.cursor.execute("select count(*) from sion.organisasi_terverifikasi")
        jumlah = conn.cursor.fetchone()[0]
        response['password'] = 'ABC'+ str(jumlah+1)
        response['no_registrasi'] = 'REG0'+str(jumlah+1)
        return HttpResponseRedirect(reverse('register:abis_org'))

def halaman_habis_regis_org(request):
    html = 'org_abis_regis.html'
    return render(request, html, response)