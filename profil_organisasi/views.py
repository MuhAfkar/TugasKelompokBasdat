from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from login.db_helper import KoneksiDb

# Create your views here.
response = {}

def index(request):

        if 'email' in request.session :
                print('logged in')
                print('di profil')
                response['login'] = True

                conn = KoneksiDb()
                conn.cursor.execute("select * from organisasi where email='"+request.session['email']+"';")
                row = conn.cursor.fetchone()
                response['email'] = row[0]
                response['website'] = row[1]
                response['namaOrganisasi'] = row[2]
                response['propinsi'] = row[3]
                response['kabupaten'] = row[4]
                response['kota'] = row[5]
                response['kodePos'] = row[6]
                conn.cursor.execute("select email from pengurus_organisasi where email='"+response['email']+"';")
                row2 = conn.cursor.fetchone()
                response['email'] = row2[0]
                print (row2)
                conn.cursor.execute("select nama from user where email='"+response['email']+"';")
                row3 = conn.cursor.fetchone()
                response['email'] = row3[0]
                print (row3)
                response['role'] = request.session['role']
                html = 'ProfilOrganisasi.html'
                return render(request, html, response)

        else :
                print('not logged in')
                response['login'] = False
                return HttpResponseRedirect(reverse('login:index'))
